package com.sehat.q.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sehat.q.model.Product
import com.sehat.q.ui.history.HistoryViewState

/**
 * Created by @erickrenata on 08/07/20.
 */

class SearchViewModel : ViewModel() {
    private val mViewState = MutableLiveData<SearchViewState>().apply {
        value = SearchViewState(null)
    }
    val viewState: LiveData<SearchViewState>
        get() = mViewState

    fun setData(product: ArrayList<Product>) {
        mViewState.value = mViewState.value?.copy(data = product)
    }
}