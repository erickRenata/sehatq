package com.sehat.q.ui.history

import com.sehat.q.model.Product


/**
 * Created by @erickrenata on 08/07/20.
 */

data class HistoryViewState (
    var data: ArrayList<Product>?
)