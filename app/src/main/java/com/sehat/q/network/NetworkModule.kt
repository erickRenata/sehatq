package com.sehat.q.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit

/**
 * Created by @erickrenata on 07/07/20.
 */
object NetworkModule {
    private const val BASE_URL = "https://private-4639ce-ecommerce56.apiary-mock.com/"

    fun create(): ApiService {
        val requestInterface = Retrofit.Builder()
            .client(provideOkHttpClient())
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return requestInterface.create(ApiService::class.java)
    }

    private fun provideOkHttpClient(): OkHttpClient? {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        val interceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message: String? ->
                Log.d(
                    "SolidApp",
                    message
                )
            })
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .cookieJar(JavaNetCookieJar(cookieManager)).addInterceptor(interceptor)
            .addNetworkInterceptor(
                object : Interceptor {
                    @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val request = chain.request()
                        val newRequest = request.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Cache-Control", "no-store")
                            .build()
                        return chain.proceed(newRequest)
                    }
                }
            )
            .build()
    }
}