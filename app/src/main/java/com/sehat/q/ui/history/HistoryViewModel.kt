package com.sehat.q.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sehat.q.model.Product

/**
 * Created by @erickrenata on 08/07/20.
 */

class HistoryViewModel : ViewModel() {
    private val mViewState = MutableLiveData<HistoryViewState>().apply {
        value = HistoryViewState(null)
    }
    val viewState: LiveData<HistoryViewState>
        get() = mViewState

    fun setData(product: ArrayList<Product>) {
        mViewState.value = mViewState.value?.copy(data = product)
    }
}