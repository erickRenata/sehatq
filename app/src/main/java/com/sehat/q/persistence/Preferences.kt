package com.sehat.q.persistence

import com.orhanobut.hawk.Hawk
import com.sehat.q.model.Product


/**
 * Created by @erickrenata on 08/07/20.
 */

class Preferences {

    object Key {
        const val PRODUCT = "Key.Product"
    }

    companion object {
        fun saveProductToCart(productList: ArrayList<Product>) {
            Hawk.put(Key.PRODUCT, productList)
        }

        fun getProductCart(): ArrayList<Product> {
            return Hawk.get(Key.PRODUCT, ArrayList())
        }

        fun removeProductCart() {
            Hawk.delete(Key.PRODUCT)
            var productList = ArrayList<Product>()
            saveProductToCart(productList)
        }
    }
}