package com.sehat.q.model

data class Category(
    val id: Int,
    val imageUrl: String,
    val name: String
)