package com.sehat.q.ui.history

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sehat.q.R
import com.sehat.q.model.Product
import com.sehat.q.ui.detail.ProductDetailAct
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*

/**
 * Created by @erickrenata on 08/07/20.
 */

class HistoryAdapter(
    private val context: Context,
    private val list: ArrayList<Product>
) :
    RecyclerView.Adapter<HistoryAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_product,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        Picasso.get().load(list[position].imageUrl).into(holder.view.iv_product)
        holder.view.tv_product_name.text = list[position].title
        holder.view.tv_product_price.text = list[position].price

        holder.view.setOnClickListener {
            context.startActivity(
                Intent(
                    Intent(context, ProductDetailAct::class.java)
                        .putExtra("product", list[position])
                )
            )
        }
    }

    class Holder(val view: View) : RecyclerView.ViewHolder(view)

}