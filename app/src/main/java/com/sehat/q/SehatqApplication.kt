package com.sehat.q

import android.app.Application
import com.orhanobut.hawk.Hawk
import com.sehat.q.network.NetworkModule

/**
 * Created by @erickrenata on 08/07/20.
 */

class SehatqApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Hawk.init(this)
            .build()
    }
}