package com.sehat.q.ui.home

import com.sehat.q.model.HomeItem

/**
 * Created by @erickrenata on 08/07/20.
 */

data class HomeViewState(
    var data: ArrayList<HomeItem>?
)