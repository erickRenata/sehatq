package com.sehat.q.network

import com.sehat.q.model.HomeItem
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by @erickrenata on 07/07/20.
 */

interface ApiService {
    @GET("home")
    fun getHome(): Observable<ArrayList<HomeItem>>
}