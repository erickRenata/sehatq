package com.sehat.q.ui.detail

import com.sehat.q.model.Product

/**
 * Created by @erickrenata on 08/07/20.
 */

data class ProductDetailViewState (
    var data: Product?
)