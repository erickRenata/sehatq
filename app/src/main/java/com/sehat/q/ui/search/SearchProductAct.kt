package com.sehat.q.ui.search

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.sehat.q.R
import com.sehat.q.model.Product
import kotlinx.android.synthetic.main.activity_search_page.*

/**
 * Created by @erickrenata on 08/07/20.
 */

class SearchProductAct : AppCompatActivity() {

    private lateinit var vm: SearchViewModel

    private lateinit var searchAdapter: SearchAdapter
    val masterProductList = ArrayList<Product>()
    val productList = ArrayList<Product>()
    val productListTemp = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_page)
        val list = intent.getParcelableArrayListExtra<Product>("product")

        initOnClickListener()
        initSearchListener()
        vm = ViewModelProviders.of(this).get(SearchViewModel::class.java).apply {
            viewState.observe(
                this@SearchProductAct,
                Observer(this@SearchProductAct::handleState)
            )
            list?.let { setData(it) }
        }

    }

    private fun initOnClickListener() {
        btn_back.setOnClickListener {
            finish()
        }
    }

    private fun handleState(viewState: SearchViewState) {
        viewState.data?.let { showDetail(it) }
    }

    private fun showDetail(data: ArrayList<Product>) {
        masterProductList.clear()
        masterProductList.addAll(data!!)
        productList.clear()
        productList.addAll(data!!)

        initAdapter()
//        searchAdapter.notifyDataSetChanged()
    }

    private fun initAdapter() {
        searchAdapter = SearchAdapter(this, productList)

        rv_search.setHasFixedSize(true)
        rv_search.layoutManager = LinearLayoutManager(this)
        rv_search.adapter = searchAdapter
    }

    private fun initSearchListener() {
        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.isNotEmpty()) {
                    productListTemp.clear()
                    for (product in masterProductList) {
                        if (product.title.toLowerCase().contains(p0.toString().toLowerCase())) {
                            productListTemp.add(product)
                        }
                    }
                    searchAdapter.replaceItem(productListTemp)
                } else {
                    searchAdapter.replaceItem(masterProductList)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //do here your stuff f
                hideSoftKeyboard(et_search)
                true
            } else false
        })
    }

    fun hideSoftKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.applicationWindowToken, 0)
    }
}