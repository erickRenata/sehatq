package com.sehat.q.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sehat.q.model.Product

/**
 * Created by @erickrenata on 08/07/20.
 */

class ProductDetailViewModel : ViewModel() {
    private val mViewState = MutableLiveData<ProductDetailViewState>().apply {
        value = ProductDetailViewState(null)
    }
    val viewState: LiveData<ProductDetailViewState>
        get() = mViewState

    fun setData(product: Product) {
        mViewState.value = mViewState.value?.copy(data = product)
    }
}