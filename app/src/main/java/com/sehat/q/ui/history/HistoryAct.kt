package com.sehat.q.ui.history

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.sehat.q.R
import com.sehat.q.model.Product
import com.sehat.q.persistence.Preferences
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_product.*
import kotlinx.android.synthetic.main.activity_detail_product.btn_back
import kotlinx.android.synthetic.main.activity_purchase_history.*
import kotlinx.android.synthetic.main.activity_search_page.*

/**
 * Created by @erickrenata on 08/07/20.
 */

class HistoryAct : AppCompatActivity() {

    private lateinit var vm: HistoryViewModel

    private lateinit var historyAdapter: HistoryAdapter
    val productList = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase_history)
        var productList = ArrayList<Product>()
        if (Preferences.getProductCart() != null) {
            productList = Preferences.getProductCart()
        }

        initOnClickListener()
        vm = ViewModelProviders.of(this).get(HistoryViewModel::class.java).apply {
            viewState.observe(
                this@HistoryAct,
                Observer(this@HistoryAct::handleState)
            )
            productList?.let { setData(it) }
        }

    }

    private fun initOnClickListener() {
        btn_back.setOnClickListener {
            finish()
        }
    }

    private fun handleState(viewState: HistoryViewState) {
        viewState.data?.let { showDetail(it) }
    }

    private fun showDetail(it: ArrayList<Product>) {
        productList.clear()
        productList.addAll(it)
        initAdapter()
    }

    private fun initAdapter() {
        historyAdapter = HistoryAdapter(this, productList)

        rv_history.setHasFixedSize(true)
        rv_history.layoutManager = LinearLayoutManager(this)
        rv_history.adapter = historyAdapter
    }
}