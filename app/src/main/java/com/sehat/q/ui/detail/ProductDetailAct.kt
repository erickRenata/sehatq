package com.sehat.q.ui.detail

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.sehat.q.R
import com.sehat.q.model.Product
import com.sehat.q.persistence.Preferences
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_product.*

/**
 * Created by @erickrenata on 08/07/20.
 */

class ProductDetailAct : AppCompatActivity() {

    private var product: Product? = null
    private lateinit var vm: ProductDetailViewModel

    var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)
        product = intent.getParcelableExtra<Product>("product")

        vm = ViewModelProviders.of(this).get(ProductDetailViewModel::class.java).apply {
            viewState.observe(
                this@ProductDetailAct,
                Observer(this@ProductDetailAct::handleState)
            )
            product?.let { setData(it) }
        }

        initOnClickListener()
    }

    private fun handleState(viewState: ProductDetailViewState) {
        viewState.data?.let { showDetail(it) }
    }

    private fun showDetail(data: Product) {
        Picasso.get()
            .load(data.imageUrl)
            .into(iv_product)

        tv_title.text = data.title
        tv_desc.text = data.description
        tv_product_price.text = data.price
    }

    private fun initOnClickListener() {
        btn_back.setOnClickListener {
            finish()
        }
        btn_share.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            val shareBody = "Here is the share content body"
            intent.type = "text/plain"
            intent.putExtra(
                Intent.EXTRA_SUBJECT,
                "Share"
            )
            intent.putExtra(Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(intent, "Share using"))
        }
        btn_wishlist.setOnClickListener {
            if (isFavorite) {
                btn_wishlist.setImageResource(R.drawable.ic_love)
            } else {
                btn_wishlist.setImageResource(R.drawable.ic_love_black)
            }
            isFavorite = !isFavorite
        }
        btn_buy.setOnClickListener {
            var productList = ArrayList<Product>()
            if (Preferences.getProductCart() == null) {
                productList.add(product!!)
                Preferences.saveProductToCart(productList)
            } else {
                productList = Preferences.getProductCart()
                productList.add(product!!)
                Preferences.saveProductToCart(productList)
            }
            Toast.makeText(this, "Pembelian berhasil", Toast.LENGTH_SHORT).show()
        }
    }
}
