package com.sehat.q.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sehat.q.R
import com.sehat.q.model.Category
import com.sehat.q.model.Product
import com.sehat.q.ui.history.HistoryAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*


/**
 * Created by @erickrenata on 08/07/20.
 */

class CategoryAdapter(
    private val context: Context,
    private val list: ArrayList<Category>
) :
    RecyclerView.Adapter<CategoryAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        Picasso.get().load(list[position].imageUrl).into(holder.view.iv_category)
        holder.view.tv_category_name.text = list[position].name
    }

    class Holder(val view: View) : RecyclerView.ViewHolder(view)

}