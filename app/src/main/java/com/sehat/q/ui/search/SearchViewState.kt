package com.sehat.q.ui.search

import com.sehat.q.model.Product

/**
 * Created by @erickrenata on 08/07/20.
 */

data class SearchViewState(
    var data: ArrayList<Product>?
)