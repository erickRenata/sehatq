package com.sehat.q.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.sehat.q.R
import com.sehat.q.model.Category
import com.sehat.q.model.HomeItem
import com.sehat.q.model.Product
import com.sehat.q.network.NetworkModule
import com.sehat.q.ui.detail.ProductDetailAct
import com.sehat.q.ui.history.HistoryAct
import com.sehat.q.ui.search.SearchProductAct
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity() {

    private lateinit var homeAdapter: HomeAdapter
    var promoList = ArrayList<Product?>()
    var categoryList = ArrayList<Category>()

    private lateinit var vm: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getDataFromAPI()
        initOnClickListener()
        bottom_navigation.setOnNavigationItemReselectedListener { item ->
            when(item.itemId) {
                R.id.page_4 -> {
                    startActivity(
                        Intent(
                            Intent(this, HistoryAct::class.java)
                        )
                    )
                }
            }
        }
    }

    private fun initOnClickListener() {
        btn_search.setOnClickListener {
            val list = ArrayList<Product?>()
            for (product in promoList){
                if (product != null){
                    list.add(product)
                }
            }
            startActivity(
                Intent(
                    Intent(this, SearchProductAct::class.java)
                        .putExtra("product", list)
                )
            )
        }
    }

    private fun initAdapter() {
        homeAdapter = HomeAdapter(this, promoList, categoryList)

        rv_home.setHasFixedSize(true)
        rv_home.layoutManager = LinearLayoutManager(this)
        rv_home.adapter = homeAdapter
    }

    private fun handleState(viewState: HomeViewState) {
        viewState.data?.let { showDetail(it) }
    }

    private fun showDetail(data: ArrayList<HomeItem>) {
        promoList.add(null)
        promoList.addAll(data[0].data.productPromo)
        categoryList.addAll(data[0].data.category)
        initAdapter()
    }

    /***** API Connection *****/
    private fun getDataFromAPI() {
        NetworkModule.create().getHome()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                handleResponse(response)
            }, { throwable ->

            })
    }

    private fun handleResponse(data: ArrayList<HomeItem>) {

        vm = ViewModelProviders.of(this).get(HomeViewModel::class.java).apply {
            viewState.observe(
                this@HomeActivity,
                Observer(this@HomeActivity::handleState)
            )
            data?.let { setData(it) }
        }
    }
}