package com.sehat.q.ui.home

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sehat.q.R
import com.sehat.q.model.Category
import com.sehat.q.model.Product
import com.sehat.q.ui.detail.ProductDetailAct
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_promo.view.*
import kotlinx.android.synthetic.main.item_rv_category.view.*

/**
 * Created by @erickrenata on 08/07/20.
 */

class HomeAdapter(
    private val context: Context,
    private val list: ArrayList<Product?>,
    private val categoryList: ArrayList<Category>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_HEADER = 100
    private val TYPE_DATA = 200

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_HEADER) {
            return HeaderHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_rv_category,
                    parent,
                    false
                )
            )
        } else {
            return DataHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_promo,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int {
        return if (list[position] == null) {
            TYPE_HEADER
        } else {
            TYPE_DATA
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DataHolder) {
            Picasso.get().load(list[position]!!.imageUrl).into(holder.view.iv_promo)
            holder.view.tv_product_name.text = list[position]!!.title

            holder.view.setOnClickListener {
                context.startActivity(
                    Intent(
                        Intent(context, ProductDetailAct::class.java)
                            .putExtra("product", list[position])
                    )
                )
            }
        } else if (holder is HeaderHolder) {
            holder.view.rv_content.setHasFixedSize(true)
            holder.view.rv_content.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            holder.view.rv_content.adapter = CategoryAdapter(context, categoryList)
        }
    }

    class DataHolder(val view: View) : RecyclerView.ViewHolder(view)

    class HeaderHolder(val view: View) : RecyclerView.ViewHolder(view)

}