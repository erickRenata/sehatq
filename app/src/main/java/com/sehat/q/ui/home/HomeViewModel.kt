package com.sehat.q.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sehat.q.model.HomeItem
import com.sehat.q.model.Product
import com.sehat.q.ui.history.HistoryViewState

/**
 * Created by @erickrenata on 08/07/20.
 */

class HomeViewModel : ViewModel() {
    private val mViewState = MutableLiveData<HomeViewState>().apply {
        value = HomeViewState(null)
    }
    val viewState: LiveData<HomeViewState>
        get() = mViewState

    fun setData(home: ArrayList<HomeItem>) {
        mViewState.value = mViewState.value?.copy(data = home)
    }
}