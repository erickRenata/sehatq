package com.sehat.q.model

data class Data(
    val category: ArrayList<Category>,
    val productPromo: ArrayList<Product>
)